const ERROR_NO_URL = 'No URL Provided';
const ERROR_MAX_LENGTH = 'Max URL size exceeded.';
const ERROR_INVALID_URL = 'Invalid url provided.';

const baseOptions = {
  url: '/s',
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json;charset=UTF-8'
  },
};

var BASE_URL = `${location.origin}/t`;


function maybeShortenURL() {
    if (window.event.keyCode === 13) {
      window.event.preventDefault();
      shortenURL()
    }
}

function shortenURL() {
  var errorEle = document.getElementById("errorMessage");
  errorEle.innerText = "";
  var urlEle = document.getElementById('url');
  var value = urlEle.value;
  if (value === "") {
    errorEle.innerText = ERROR_NO_URL;
  }
  else if (value.length > 1023) {
    errorEle.innerText = ERROR_MAX_LENGTH;
  }
  else if (value.indexOf("http://") === -1 && value.indexOf("https://") === -1) {
    errorEle.innerText = ERROR_INVALID_URL;
  }  
  else {
    var clonedOptions = Object. assign({}, baseOptions)
    clonedOptions.data = {url: value};
    axios(clonedOptions)
    .then(response => {
      var shortenURL = response.data.shortenedURL;
      var shortenedEle = document.getElementById("shortenedUrl");
      var href = [BASE_URL, shortenURL].join('/');
      shortenedEle.innerHTML = `Zero Hassle URL: <a id='urlOnly' href='${href}' target="_blank">${href}<a/>`
      errorEle.value = "";
      urlEle.value = "";
      document.getElementById("url-entry").style.display="none";
      document.getElementById("clip-btn").style.display="inline";
      document.getElementById("anotherOne").style.display="inline";
      
      
    });

  }
}

document.onreadystatechange = function () {
  if (document.readyState == "interactive") {
    let selector = 'button.clip-btn';
    var clipboard = new ClipboardJS(selector, {
      text: function() {
          var htmlBlock = document.querySelector('#urlOnly');
          return htmlBlock.innerText;
      }
    });
    clipboard.on('success', function(e) {
      e.clearSelection();      
    });
  
  }
};
