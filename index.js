const logger = require('pino')()
const fs = require("fs"),
      path = require("path");

const fastify = require('fastify')({
  logger: {
    level: 'info',
    file: './storage/shortener.log'
  }
})

fastify.register(require('point-of-view'), {
  engine: {
    nunjucks: require('nunjucks')
  }
})

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/public/',
})

const PORT = 8934;
const MAX_URL_LENGTH = 16384;
const ID_LEN = 4;
const BASE_PATH = "./data";
const DATA_FILE = "orig_url.data";

const ERROR_NO_URL = 'No URL Provided';
const ERROR_MAX_LENGTH = 'Max URL size exceeded.';
const ERROR_ERROR_OCCURED ="Error occured, please try again later."

function showHomePage(reply, shortenedURL, originalURL, errorMessage){
  reply.send({ shortenedURL: shortenedURL,
               originalURL : originalURL,
               errorMessage :errorMessage   })  
}

fastify.get('/favicon.ico', (request, reply) => {
    const icon = path.join('favicon.ico')
    fs.readFile(icon, (err, data) => {
      let stream = fs.createReadStream('favicon.ico')
      reply.type('image/x-icon').send(stream)
    })
})

fastify.get('/robots.txt', function (req, reply) {
    const robots = path.join('./public', 'robots.txt')
    fs.readFile(robots, (err, data) => {
      let stream = fs.createReadStream('./public/robots.txt')
      reply.type('text/plain').send(stream)
    })
})

fastify.get('/', (request, reply) => {
  reply.view('/templates/index.njk');
})

fastify.get('/t/:shortened', (request, reply) => {
  var id = request.params['shortened'];
  var yearPart = id.substring(0,2);
  var folder = id.substring(2,id.length);
  if (yearPart != "20") {
      yearPart = id.substring(0, 1);
      folder = id.substring(1, id.length);
  }
  var path = [BASE_PATH, yearPart, folder].join("/");
  if (!fs.existsSync(path)) {
      return reply.send({ URL_NOT_FOUND: 'walla' });
  } else {
    fs.readFile(`${path}/${DATA_FILE}`, function(err, data) {
        reply.header('Location', data)
        reply.code(302)
        return reply.send();
     });
  }
})

fastify.post('/s', (request, reply) => {
  var urlToShorten = request.body['url'];
  if (!urlToShorten) {
      return showHomePage(reply, '', urlToShorten, ERROR_NO_URL);
  }
  else if (urlToShorten.length > MAX_URL_LENGTH) {
      return showHomePage(reply, '', urlToShorten, ERROR_MAX_LENGTH);
  }

  var goodId = false;
  while (goodId === false){
      var urlID = urlShortner(ID_LEN);
      var yearAsChar = yearToSingleLetter()
      var partitions = [yearAsChar, urlID];
      var path = [BASE_PATH, yearAsChar, urlID].join("/");
      if (!fs.existsSync(path)) {
          goodId = true
          fs.mkdir(path, { recursive: true }, (err) => {
            if (err) { 
              console.error(err); 
              return showHomePage(reply, '', urlToShorten, ERROR_ERROR_OCCURED);
            } else {
                fs.writeFile(`${path}/${DATA_FILE}`, urlToShorten , function (err, file) {
                   if (err) throw err;
                      return showHomePage(reply, partitions.join(""), urlToShorten, null)
                 });
            }

          });
       }
  }
})

fastify.get('*', function (request, reply) {
    reply.view('/templates/index.njk')
})

function urlShortner(len) {
    var len = len || 4;
    var gen = Math.random().toString(36).slice(2);
    var part1 = gen.substring(0, 4).toUpperCase();
    var part2 = gen.substring(5, gen.length);
    return (part1+part2).split('').sort(function(){return 0.5-Math.random()}).join('').substring(0, len)
}

function yearToSingleLetter() {
    var BASE_ASCII = 83;
    var year = new Date().getFullYear() - 2000;
    return String.fromCharCode(BASE_ASCII + year);
}


async function runServer() {
  const address = await fastify.listen(PORT);
  await fastify.ready();
  let message = `server listening on ${address}`;
  fastify.log.info(`server listening on ${address}`);
  console.log(message);
}

runServer()
